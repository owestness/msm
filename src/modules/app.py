from textual.app import App, ComposeResult
from textual.binding import Binding
from textual.widgets import Footer, DataTable

class App(App):
    BINDINGS = [
            Binding(key="q", action="quit", description="Quit")
            ]

    def compose(self) -> ComposeResult:
        yield DataTable()
        yield Footer()

def msm():
    app = App()
    app.run()
